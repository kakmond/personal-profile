//
//  SkillsViewController.swift
//  personal profile
//
//  Created by Mond on 19/11/2562 BE.
//  Copyright © 2562 mond. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {
    
    @IBOutlet weak var animationView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 3, delay: 0, options: [.curveEaseInOut, .preferredFramesPerSecond60], animations: {
            self.animationView.frame.origin.y -= 200
            self.animationView.frame.origin.x -= 125
            self.animationView.backgroundColor = UIColor.yellow
        }) { (completed) in
            if completed {
                self.advancedAnimation()
            } else {
                print("Animation finised")
            }
            
        }
    }
    
    @IBAction func dismissButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func advancedAnimation() {
        let startPosition = animationView.center
        UIView.animateKeyframes(withDuration: 4, delay: 0, options: [.repeat, .autoreverse], animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1) {
                self.animationView.backgroundColor = UIColor.blue
            }
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.2) {
                self.animationView.center = self.view.center
            }
            
            UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.3) {
                self.animationView.transform = CGAffineTransform(rotationAngle: CGFloat.pi).concatenating(CGAffineTransform(scaleX: 1.5 , y: 1.5))
            }
            
            UIView.addKeyframe(withRelativeStartTime: 0.6, relativeDuration: 0.4) {
                self.animationView.center = startPosition
                self.animationView.transform = CGAffineTransform.identity
            }
        }) { (completed) in
            print("Completed: \(completed)")
        }
    }
}
