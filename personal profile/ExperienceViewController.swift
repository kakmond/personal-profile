//
//  ExperienceViewController.swift
//  personal profile
//
//  Created by Mond on 30/10/2562 BE.
//  Copyright © 2562 mond. All rights reserved.
//

import UIKit

class ExperienceViewController: UITableViewController {
    
    let works = [
        Experience(title: "UniNews", imgPath: "work1", date: "August 2018", description: "Developed a news mobile application for  propagation  news  in  Kasetsart  University that  students  can  catch  up  and  never  miss  the  latest  news at Kasetsart  University."),
        Experience(title: "D'alessio", imgPath: "work2", date: "May 2016", description: "Developed a shopping cart web application that integrates with Paypal using Express.js"),
    ]
    
    let educations = [
        Experience(title: "Kasetsart university", imgPath: "education2", date: "2016 - Present", description: " Currently, I am third year student in software and knowledge engineering at Kasetsart  University in Thailand."),
        Experience(title: "Satriwitthaya 2 school", imgPath: "education1", date: "2010 - 2016", description: "Studied the science program at Satriwitthaya 2 school in Thailand"),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let controller = segue.destination as! ExperienceDetailViewController
                let experience: Experience
                if indexPath.section == 0 {
                    experience = works[indexPath.row]
                } else {
                    experience = educations[indexPath.row]
                }
                controller.experience = experience
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return works.count
        } else {
            return educations.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        if indexPath.section == 0 {
            let work = works[indexPath.row]
            cell.titleLabel?.text = work.title
            cell.dateLabel?.text = work.date
            cell.imgView?.image = UIImage(named: work.imgPath)
            return cell
        } else {
            let work = educations[indexPath.row]
            cell.titleLabel?.text = work.title
            cell.dateLabel?.text = work.date
            cell.imgView?.image = UIImage(named: work.imgPath)
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Work"
        } else {
            return "Education"
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
}
