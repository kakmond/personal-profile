//
//  Experience.swift
//  personal profile
//
//  Created by Mond on 1/11/2562 BE.
//  Copyright © 2562 mond. All rights reserved.
//

import Foundation

class Experience
{
    var title: String
    var imgPath: String
    var date: String
    var description: String
    
    init(title: String, imgPath: String, date: String, description: String){
        self.title = title
        self.imgPath = imgPath
        self.description = description
        self.date = date
    }
}
