//
//  ExperienceDetailViewController.swift
//  personal profile
//
//  Created by Mond on 1/11/2562 BE.
//  Copyright © 2562 mond. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var experience: Experience!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = experience.title
        titleLabel.text = experience.title
        descriptionLabel.text = experience.description
        dateLabel.text = experience.date
        imgView.image = UIImage(named: experience.imgPath)
    }
}
